"use strict";
const path = require('path');

/**
 * Contains a list of loaders for Webpack to preprocess files.
 */
module.exports = [
    {
        test: /\.jsx?$/,
        include: path.resolve(__dirname, 'src'),
        loader: "babel-loader",
        options: {
            cacheDirectory: true,
            plugins: ['react-hot-loader/babel'],
        }
    },
    {
        test:/\.(s*)css$/,
        use:['style-loader','css-loader', 'sass-loader']
    }
];
