import {
    getBasicInfo,
    getFollowers,
    getFollowing,
    getProjects,
    getStats,
    getWorkExperience
} from "../api/behance/UsersAPI";
import {
    fetchBasicInfoErrorAction,
    fetchBasicInfoSuccessAction,
    fetchFollowersError,
    fetchFollowersSuccess,
    fetchFollowingError,
    fetchFollowingSuccess,
    fetchProjectsError,
    fetchProjectsSuccess,
    fetchStatsErrorAction,
    fetchStatsSuccessAction,
    fetchWorkExperienceError,
    fetchWorkExperienceSuccess
} from "./UserActions";

/**
 * Action wrapper to fetch user's basic info data from API and dispatch action to update the store.
 * @param username
 * @returns {Function}
 */
export const fetchBasicInfo = (username) => async (dispatch) => {
    try {
        const data = await getBasicInfo(username);
        return dispatch(fetchBasicInfoSuccessAction(data));
    } catch (e) {
        console.log("Error fetching user's basic info", e);
        return dispatch(fetchBasicInfoErrorAction(e));
    }
};

/**
 * Action wrapper to fetch user's stats data from API and dispatch action to update the store.
 * @param username
 * @returns {Function}
 */
export const fetchStats = (username) => async (dispatch) => {
    try {
        const data = await getStats(username);
        return dispatch(fetchStatsSuccessAction(data));
    } catch (e) {
        console.log("Error fetching user's stats", e);
        return dispatch(fetchStatsErrorAction(e));
    }
};

/**
 * Action wrapper to fetch user's projects data from API and dispatch action to update the store.
 * @param username
 * @returns {Function}
 */
export const fetchProjects = (username) => async (dispatch) => {
    try {
        const data = await getProjects(username);
        return dispatch(fetchProjectsSuccess(data));
    } catch (e) {
        console.log("Error fetching user's projects", e);
        return dispatch(fetchProjectsError(e));
    }
};

/**
 * Action wrapper to fetch user's work experience data from API and dispatch action to update the store.
 * @param username
 * @returns {Function}
 */
export const fetchWorkExperience = (username) => async (dispatch) => {
    try {
        const data = await getWorkExperience(username);
        return dispatch(fetchWorkExperienceSuccess(data));
    } catch (e) {
        console.log("Error fetching user's work experience", e);
        return dispatch(fetchWorkExperienceError(e));
    }
};

/**
 * Action wrapper to fetch user's followers data from API and dispatch action to update the store.
 * @param username
 * @returns {Function}
 */
export const fetchFollowers = (username) => async (dispatch) => {
    try {
        const data = await getFollowers(username);
        return dispatch(fetchFollowersSuccess(data));
    } catch (e) {
        console.log("Error fetching user's followers", e);
        return dispatch(fetchFollowersError(e));
    }
};

/**
 * Action wrapper to fetch user's followees data from API and dispatch action to update the store.
 * @param username
 * @returns {Function}
 */
export const fetchFollowing = (username) => async (dispatch) => {
    try {
        const data = await getFollowing(username);
        return dispatch(fetchFollowingSuccess(data));
    } catch (e) {
        console.log("Error fetching user's followees", e);
        return dispatch(fetchFollowingError(e));
    }
};

