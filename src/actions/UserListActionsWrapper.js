import {getUserList} from "../api/behance/UsersAPI";
import {fetchUserListErrorAction, fetchUserListSuccessAction} from "./UserListActions";

/**
 * Action wrapper to fetch a list of users data from API and dispatch action to update the store.
 * Instead of updating the store directly in the API success response,
 * this allows future use of API without always saving it the store.
 * @param username
 * @returns {Function}
 */
export const fetchUserList = (params) => async (dispatch) => {
    try {
        const data = await getUserList(params);
        return dispatch(fetchUserListSuccessAction(data));
    } catch (e) {
        console.log("Error fetching user list", e);
        return dispatch(fetchUserListErrorAction(e));
    }
};
