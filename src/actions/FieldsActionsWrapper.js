import {fetchFieldsErrorAction, fetchFieldsSuccessAction} from "./FieldsActions";
import {getFields} from 'api/behance/FieldsAPI';

/**
 * Action wrapper to fetch fields data from API and dispatch action to update the store.
 * Instead of updating the store directly in the API success response,
 * this allows future use of API without always saving it the store.
 * @returns {Function}
 */
export const fetchFields = () => async (dispatch) => {
    try {
        const data = await getFields();
        return dispatch(fetchFieldsSuccessAction(data));
    } catch (e) {
        console.log("Error fetching fields", e);
        return dispatch(fetchFieldsErrorAction(e));
    }
};
