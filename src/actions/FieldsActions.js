import {ActionTypes} from "./ActionTypes";

/**
 * Fields Action Creators
 */
export const fetchFieldsSuccessAction = (fields) => {
    return {
        type: ActionTypes.FETCH_FIELDS_SUCCESS,
        payload: fields
    };
};

export const fetchFieldsErrorAction = (err) => {
    return {
        type: ActionTypes.FETCH_BASIC_INFO_ERROR,
        payload: err
    };
};
