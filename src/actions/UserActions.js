import {ActionTypes} from "./ActionTypes";

/**
 * User Action Creators
 */
export const resetUser = () => {
    return {
        type: ActionTypes.RESET_USER,
        payload: null
    };
};

export const fetchBasicInfoSuccessAction = (user) => {
    return {
        type: ActionTypes.FETCH_BASIC_INFO_SUCCESS,
        payload: user
    };
};

export const fetchBasicInfoErrorAction = (err) => {
    return {
        type: ActionTypes.FETCH_BASIC_INFO_ERROR,
        payload: err
    };
};

export const fetchStatsSuccessAction = (stats) => {
    return {
        type: ActionTypes.FETCH_STATS_SUCCESS,
        payload: stats
    };
};

export const fetchStatsErrorAction = (err) => {
    return {
        type: ActionTypes.FETCH_STATS_ERROR,
        payload: err
    };
};

export const fetchProjectsSuccess = (projects) => {
    return {
        type: ActionTypes.FETCH_PROJECTS_SUCCESS,
        payload: projects
    };
};

export const fetchProjectsError = (err) => {
    return {
        type: ActionTypes.FETCH_PROJECTS_ERROR,
        payload: err
    };
};

export const fetchWorkExperienceSuccess = (workExperience) => {
    return {
        type: ActionTypes.FETCH_WORK_EXPERIENCE_SUCCESS,
        payload: workExperience
    };
};

export const fetchWorkExperienceError = (err) => {
    return {
        type: ActionTypes.FETCH_WORK_EXPERIENCE_ERROR,
        payload: err
    };
};

export const fetchFollowersSuccess = (followers) => {
    return {
        type: ActionTypes.FETCH_FOLLOWERS_SUCCESS,
        payload: followers
    };
};

export const fetchFollowersError = (err) => {
    return {
        type: ActionTypes.FETCH_FOLLOWERS_ERROR,
        payload: err
    };
};

export const fetchFollowingSuccess = (following) => {
    return {
        type: ActionTypes.FETCH_FOLLOWING_SUCCESS,
        payload: following
    };
};

export const fetchFollowingError = (err) => {
    return {
        type: ActionTypes.FETCH_FOLLOWING_ERROR,
        payload: err
    };
};
