import {ActionTypes} from "./ActionTypes";

/**
 * UserList Action Creators
 */
export const resetUserList = () => {
    return {
        type: ActionTypes.RESET_USER_LIST,
        payload: null
    };
};

export const fetchUserListSuccessAction = (userList) => {
    return {
        type: ActionTypes.FETCH_USER_LIST_SUCCESS,
        payload: userList
    };
};

export const fetchUserListErrorAction = (err) => {
    return {
        type: ActionTypes.FETCH_USER_LIST_ERROR,
        payload: err
    };
};
