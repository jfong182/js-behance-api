import React, {Component} from 'react';
import PropTypes from "prop-types";
import '../stylesheets/FollowerList.scss';
import {fetchFollowers} from "../../actions/UserActionsWrapper";
import connect from "react-redux/es/connect/connect";

const NO_OF_PREVIEW = 5; // specifies the number of users in the preview.

/**
 * Follower List - displays a preview list of user's followers.
 */
class FollowerList extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.username) {
            this.props.fetchFollowers(this.props.username);
        }
    }

    render() {
        const followers = this.props.user && this.props.user.followers;

        return (
            <div className={"follower-list"}>
                <div className={"header"}>Here is a preview of the user's followers.</div>
                <table>
                    <thead>
                    <tr>
                        <th className={"name-col"}>Name</th>
                        <th className={"location-col"}>Location</th>
                        <th className={"company-col"}>Company</th>
                        <th className={"occupation-col"}>Occupation</th>
                        <th className={"no-of-followers-col"}>Number of Followers</th>
                    </tr>
                    </thead>
                    <tbody>
                    {followers && followers.splice(0, NO_OF_PREVIEW).map((f, i) =>
                        (<tr key={i}>
                            <td><a href={f.url} target={"_blank"}>{f.displayName}</a></td>
                            <td>{f.location}</td>
                            <td>{f.company}</td>
                            <td>{f.occupation}</td>
                            <td>{f.noOfFollowers}</td>
                        </tr>))
                    }
                    </tbody>
                </table>
                {!followers.length && <div className={"no-results"}>No followers have been found for the user.</div>}
            </div>
        );
    }
}

FollowerList.propTypes = {
    username: PropTypes.string // the user argument for Behance API
};

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchFollowers: (username) => dispatch(fetchFollowers(username))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FollowerList);

