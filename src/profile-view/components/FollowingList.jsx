import React, {Component} from 'react';
import PropTypes from "prop-types";
import '../stylesheets/FollowingList.scss';
import connect from "react-redux/es/connect/connect";
import {fetchFollowing} from "../../actions/UserActionsWrapper";

const NO_OF_PREVIEW = 5; // specifies the number of users in the preview.

/**
 * Following List - displays a preview list of other users that the user follows.
 */
class FollowingList extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.username) {
            this.props.fetchFollowing(this.props.username);
        }
    }

    render() {
        const following = this.props.user && this.props.user.following;

        return (
            <div className={"following-list"}>
                <div className={"header"}>Here is a preview of other users the user is following.</div>
                <table>
                    <thead>
                    <tr>
                        <th className={"name-col"}>Name</th>
                        <th className={"location-col"}>Location</th>
                        <th className={"company-col"}>Company</th>
                        <th className={"occupation-col"}>Occupation</th>
                        <th className={"no-of-followers-col"}>Number of Followers</th>
                    </tr>
                    </thead>
                    <tbody>
                    {following && following.splice(0, NO_OF_PREVIEW).map((f, i) =>
                        (<tr key={i}>
                            <td><a href={f.url} target={"_blank"}>{f.displayName}</a></td>
                            <td>{f.location}</td>
                            <td>{f.company}</td>
                            <td>{f.occupation}</td>
                            <td>{f.noOfFollowers}</td>
                        </tr>))
                    }
                    </tbody>
                </table>
                {!following.length && <div className={"no-results"}>No followees have been found for the user.</div>}
            </div>
        );
    }
}

FollowingList.propTypes = {
    username: PropTypes.string // the user argument for Behance API
};

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchFollowing: (username) => dispatch(fetchFollowing(username))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FollowingList);

