import React, {Component} from "react";
import PropTypes from "prop-types";
import UserInfo from "./UserInfo";
import ProjectList from "./ProjectList";
import WorkExperience from "./WorkExperience";
import FollowerList from "./FollowerList";
import FollowingList from "./FollowingList";
import Tabs from "../../ui-components/Tabs";
import '../stylesheets/ProfileView.scss';
import connect from "react-redux/es/connect/connect";
import {resetUser} from 'actions/UserActions';

/**
 * Profile View - a container of different sets of information pertaining the user.
 */
class ProfileView extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.resetUser();
        this.props.history.push('/');
    }

    render() {
        const username = this.props.match.params.username;

        return (
            <div className={"profile"}>
                <div className={"back-to-search"} onClick={this.handleClick}>&#x21E6; BACK TO SEARCH
                </div>
                <h1>User Profile</h1>
                <Tabs>
                    <UserInfo username={username} label={"Basic Info"}/>
                    <ProjectList username={username} label={"Project List"}/>
                    <WorkExperience username={username} label={"Work Experience"}/>
                    <FollowerList username={username} label={"Followers"}/>
                    <FollowingList username={username} label={"Following List"}/>
                </Tabs>
            </div>
        );
    }
}

ProfileView.propTypes = {
    match: PropTypes.object, // the "match" object from React Router which contains information about the URL
    history: PropTypes.object // the "history" object from React Router which contains navigating functions
};

const mapDispatchToProps = dispatch => {
    return {
        resetUser: () => dispatch(resetUser())
    };
};

export default connect(null, mapDispatchToProps)(ProfileView);

