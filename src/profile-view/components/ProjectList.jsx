import React, {Component} from 'react';
import PropTypes from "prop-types";
import '../stylesheets/ProjectList.scss';
import {fetchProjects} from "../../actions/UserActionsWrapper";
import connect from "react-redux/es/connect/connect";

/**
 * Project List - a list of user's projects.
 */
class ProjectList extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.username) {
            this.props.fetchProjects(this.props.username);
        }
    }

    render() {
        const projects = this.props.user && this.props.user.projects;

        return (
            <div className={"project-list"}>
                <table>
                    <thead>
                    <tr>
                        <th className={"preview-col"}>Preview</th>
                        <th className={"name-col"}>Name</th>
                        <th className={"published-on-col"}>Published On</th>
                        <th className={"no-of-views-col"}>Number of Views</th>
                        <th className={"no-of-appreciations-col"}>Number of Appreciations</th>
                    </tr>
                    </thead>
                    <tbody>
                    {projects && projects.map((p, i) =>
                        (<tr key={i}>
                            <td><img src={p.imageUrl}/></td>
                            <td>
                                <a href={p.url} target={"_blank"}>{p.name}</a>
                            </td>
                            <td>{p.publishedOn ? new Date(p.publishedOn).toLocaleDateString() : ''}</td>
                            <td>{p.noOfViews}</td>
                            <td>{p.noOfAppreciations}</td>
                        </tr>))
                    }
                    </tbody>
                </table>
                {!projects.length && <div className={"no-results"}>No projects have been found for the user.</div>}
            </div>
        );
    }
}

ProjectList.propTypes = {
    username: PropTypes.string // the user argument for Behance API
};

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchProjects: (username) => dispatch(fetchProjects(username))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectList);
