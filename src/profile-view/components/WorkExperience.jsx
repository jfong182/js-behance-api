import React, {Component} from 'react';
import PropTypes from "prop-types";
import '../stylesheets/WorkExperience.scss';
import {fetchWorkExperience} from "../../actions/UserActionsWrapper";
import connect from "react-redux/es/connect/connect";

/**
 * Work experience - a list of user's work experience.
 */
class WorkExperience extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.username) {
            this.props.fetchWorkExperience(this.props.username);
        }
    }

    render() {
        const workExperience = this.props.user && this.props.user.workExperience;

        return (
            <div className={"work-experience"}>
                <table>
                    <thead>
                    <tr>
                        <th className={"position-col"}>Position</th>
                        <th className={"organization-col"}>Organization</th>
                        <th className={"location-col"}>Location</th>
                        <th className={"start_date-col"}>Start Date</th>
                        <th className={"end_date-col"}>End Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    {workExperience && workExperience.map((w, i) =>
                        (<tr key={i}>
                            <td>{w.position}</td>
                            <td>{w.organization}</td>
                            <td>{w.location}</td>
                            <td>{w.startDate}</td>
                            <td>{w.endDate}</td>
                        </tr>))
                    }
                    </tbody>
                </table>
                {!workExperience.length &&
                <div className={"no-results"}>No work experience has been found for the user.</div>}
            </div>
        );
    }
}

WorkExperience.propTypes = {
    username: PropTypes.string // the user argument for Behance API
};

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchWorkExperience: (username) => dispatch(fetchWorkExperience(username))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(WorkExperience);
