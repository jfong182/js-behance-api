import React, {Component} from 'react';
import PropTypes from "prop-types";
import '../stylesheets/UserInfo.scss';
import {connect} from 'react-redux';
import {fetchBasicInfo, fetchStats} from "../../actions/UserActionsWrapper";

/**
 * Basic Info - displays user info and stats.
 */
class UserInfo extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.username) {
            this.props.fetchBasicInfo(this.props.username);
            this.props.fetchStats(this.props.username);
        }
    }

    render() {
        const basicInfo = this.props.user && this.props.user.basicInfo;
        const stats = this.props.user && this.props.user.stats;

        return (
            <div className={"basic-info"}>
                <div className={"basic-info__profile-pic-container"}>
                    <div className={"profile-pic"}>
                        {basicInfo && basicInfo.imageUrl ? <img src={basicInfo.imageUrl} alt={"Profile Picture"}/> :
                            <div>No Profile Image Available</div>}
                    </div>
                </div>
                <div className={"basic-info__content"}>
                    {basicInfo &&
                        <div className={"user-info"}>
                            <div><label>First Name:</label> {basicInfo.firstName}</div>
                            <div><label>Last Name:</label> {basicInfo.lastName}</div>
                            <div><label>Location:</label> {basicInfo.location}</div>
                            <div><label>Company:</label> {basicInfo.company}</div>
                            <div><label>Occupation:</label> {basicInfo.occupation}</div>
                            <div><label>Fields:</label> {basicInfo.fields && basicInfo.fields.map((field, i) =>
                                <span key={i}>{field}</span>)}
                            </div>
                            <div className={"social-links"}>
                                <label>Social Links:</label>
                                {basicInfo.socialLinks && basicInfo.socialLinks.map((link, i) =>
                                    <a key={i} href={link.url} target={"_blank"}>{link.service_name}</a>)}
                            </div>
                        </div>}
                    {stats &&
                        <div className={"stats"}>
                            <ul><label>All-time:</label>
                                <li><label>Project Views:</label> {stats.allTimeProjectViews}</li>
                                <li><label>Project Appreciations:</label> {stats.allTimeProjectAppreciations}</li>
                                <li><label>Project Comments:</label> {stats.allTimeProjectComments}</li>
                                <li><label>Profile Views:</label> {stats.allTimeProfileViews}</li>
                            </ul>
                            <ul><label>Today:</label>
                                <li><label>Project Views:</label> {stats.todayProjectViews}</li>
                                <li><label>Project Appreciations:</label> {stats.todayProjectAppreciations}</li>
                                <li><label>Project Comments:</label> {stats.todayProjectComments}</li>
                                <li><label>Profile Views:</label> {stats.todayProfileViews}</li>
                            </ul>
                        </div>}
                </div>
            </div>
        );
    }
}

UserInfo.propTypes = {
    username: PropTypes.string // the user argument for Behance API
};

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchBasicInfo: (username) => dispatch(fetchBasicInfo(username)),
        fetchStats: (username) => dispatch(fetchStats(username))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserInfo);



