import Tab from '../ui-components/Tab';
import {configure, shallow} from 'enzyme';
import * as React from 'react';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

const mockCallback = jest.fn((label) => {
});
const labelText = 'labelText';

describe('<Tab />', () => {

    let wrapper;
    beforeEach(() => {
        wrapper = shallow(
            (<Tab label={labelText} activeTab={labelText}
                  onClick={mockCallback}/>)
        );
    });

    test('Tab class changes to active when label matches the active tab', () => {
        wrapper.find('li').simulate('click');
        expect(wrapper.find('li').hasClass('tab-list-active')).toBe(true);
    });

    test('Tab class remains unchanged when label does not match the active tab', () => {
        wrapper = shallow(<Tab label={labelText} activeTab={"differentLabelText"}
                               onClick={() => {
                               }}></Tab>);
        wrapper.find('li').simulate('click');
        expect(wrapper.find('li').hasClass('tab-list-active')).toBe(false);
    });

    test('onClick function is called when Tab is clicked on', () => {
        wrapper.find('li').simulate('click');
        expect(mockCallback.mock.calls[0][0]).toBe(labelText);
    });
});


