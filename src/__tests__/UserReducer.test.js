import {initialState, updateUser as reducer} from '../reducers/UserReducer';
import {ActionTypes} from '../actions/ActionTypes';
import {basicInfo} from '../__mocks__/BasicInfo';
import {stats} from '../__mocks__/Stats';
import {projectList} from '../__mocks__/ProjectList';
import {workExperience} from '../__mocks__/WorkExperience';
import {followers} from '../__mocks__/Followers';
import {following} from '../__mocks__/Following';
import * as React from "react";

describe('UserReducer', () => {

    let reducerState;
    beforeEach(() => {
        reducerState = {...initialState};
    });

    test('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    test(`should handle ${ActionTypes.FETCH_BASIC_INFO_SUCCESS} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.FETCH_BASIC_INFO_SUCCESS,
                payload: basicInfo
            })
        ).toEqual({...initialState, basicInfo: basicInfo});
    });

    test(`should handle ${ActionTypes.FETCH_BASIC_INFO_ERROR} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.FETCH_BASIC_INFO_ERROR,
                payload: {error: "error response"}
            })
        ).toEqual({...initialState, basicInfo: null});
    });

    test(`should handle ${ActionTypes.FETCH_STATS_SUCCESS} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.FETCH_STATS_SUCCESS,
                payload: stats
            })
        ).toEqual({...initialState, stats: stats});
    });


    test(`should handle ${ActionTypes.FETCH_STATS_ERROR} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.FETCH_STATS_ERROR,
                payload: {error: "error response"}
            })
        ).toEqual({...initialState, stats: null});
    });

    test(`should handle ${ActionTypes.FETCH_PROJECTS_SUCCESS} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.FETCH_PROJECTS_SUCCESS,
                payload: projectList
            })
        ).toEqual({...initialState, projects: projectList});
    });

    test(`should handle ${ActionTypes.FETCH_PROJECTS_ERROR} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes,
                payload: {error: "error response"}
            })
        ).toEqual({...initialState, projects: []});
    });

    test(`should handle ${ActionTypes.FETCH_WORK_EXPERIENCE_SUCCESS} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.FETCH_WORK_EXPERIENCE_SUCCESS,
                payload: workExperience
            })
        ).toEqual({...initialState, workExperience: workExperience});
    });

    test(`should handle ${ActionTypes.FETCH_WORK_EXPERIENCE_ERROR} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.FETCH_WORK_EXPERIENCE_ERROR,
                payload: {error: "error response"}
            })
        ).toEqual({...initialState, workExperience: []});
    });

    test(`should handle ${ActionTypes.FETCH_FOLLOWERS_SUCCESS} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.FETCH_FOLLOWERS_SUCCESS,
                payload: followers
            })
        ).toEqual({...initialState, followers: followers});
    });

    test(`should handle ${ActionTypes.FETCH_FOLLOWERS_ERROR} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.FETCH_FOLLOWERS_ERROR,
                payload: {error: "error response"}
            })
        ).toEqual({...initialState, followers: []});
    });

    test(`should handle ${ActionTypes.FETCH_FOLLOWING_SUCCESS} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.FETCH_FOLLOWING_SUCCESS,
                payload: following
            })
        ).toEqual({...initialState, following: following});
    });

    test(`should handle ${ActionTypes.FETCH_FOLLOWING_ERROR} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.FETCH_FOLLOWING_ERROR,
                payload: {error: "error response"}
            })
        ).toEqual({...initialState, following: []});
    });

    test(`should handle ${ActionTypes.RESET_USER} correctly`, () => {
        expect(
            reducer(reducerState, {
                type: ActionTypes.RESET_USER,
                payload: null
            })
        ).toEqual({...initialState});
    });

});
