import {configure, shallow} from 'enzyme';
import * as React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import SimpleDropDown from "../ui-components/SimpleDropDown";

configure({adapter: new Adapter()});

const defaultOption = {id: 1, name: 'defaultOption'};
const options = [...defaultOption, {id: 2, name: 'option2'}, {id: 3, name: 'option3'}];
const mockCallback = jest.fn((selectedId) => {
});

describe('<SimpleDropDown />', () => {

    let wrapper;
    beforeEach(() => {
        wrapper = shallow(
            <SimpleDropDown selected={defaultOption}
                            options={options}
                            onSelect={mockCallback}>
            </SimpleDropDown>);
    });

    test('Simple Drop Down has the correct default value', () => {
        expect(wrapper.find('.selected').text()).toBe(defaultOption.name);
    });

    test('Simple Drop Down has menu hidden initially', () => {
        expect(wrapper.exists('.menu')).toBe(false);
    });

    test('Simple Drop Down opens up the menu upon clicking on the trigger', () => {
        wrapper.find('.simple-drop-down__trigger').simulate('click');
        expect(wrapper.exists('.menu')).toBe(true);
    });

    test('Simple Drop Down has the correct options on the menu', () => {
        wrapper.find('.simple-drop-down__trigger').simulate('click');
        expect(wrapper.exists('.menu')).toBe(true);
        const menuOptions = wrapper.find('.menu li');
        menuOptions.map((op, i) => expect(op.text()).toBe(options[i].name));
    });

    test('Simple Drop Down invokes onSelect with the correct option id and closes the menu', () => {
        wrapper.find('.simple-drop-down__trigger').simulate('click');
        expect(wrapper.exists('.menu')).toBe(true);
        const menuOptions = wrapper.find('.menu li');
        menuOptions.last().simulate('click');
        expect(mockCallback.mock.calls[0][0]).toBe(options[options.length - 1].id);
        expect(wrapper.exists('.menu')).toBe(false);
    });
});
