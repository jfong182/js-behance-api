import {updateUserList as reducer} from '../reducers/UserListReducer';
import {ActionTypes} from '../actions/ActionTypes';
import {users} from '../__mocks__/Users';

describe('UserListReducer', () => {
    test('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual([]);
    });

    test(`should handle ${ActionTypes.FETCH_USER_LIST_SUCCESS} correctly`, () => {
        expect(
            reducer([], {
                type: ActionTypes.FETCH_USER_LIST_SUCCESS,
                payload: users
            })
        ).toEqual(users);
    });

    test(`should handle ${ActionTypes.FETCH_USER_LIST_ERROR} correctly`, () => {
        expect(
            reducer([], {
                type: ActionTypes.FETCH_USER_LIST_ERROR,
                payload: {error: "error response"}
            })
        ).toEqual([]);
    });

    test(`should handle ${ActionTypes.RESET_USER_LIST} correctly`, () => {
        expect(
            reducer([], {
                type: ActionTypes.RESET_USER_LIST,
                payload: null
            })
        ).toEqual([]);
    });
});
