/**
 * Removes whitespaces and returns an encoded URI component.
 * @param str
 * @returns {string}
 */
export const sanitize = (input) => {
    return encodeURIComponent(input.toString().replace(/\s/g, ''));
};
