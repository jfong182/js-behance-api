import SimpleDropDown from "../../ui-components/SimpleDropDown";
import React, {Component} from "react";
import '../stylesheets/SearchControls.scss';
import {fetchFields} from "../../actions/FieldsActionsWrapper";
import connect from "react-redux/es/connect/connect";
import PropTypes from "prop-types";
import {COUNTRY_LIST} from "../../constants/CountryList";

/**
 * Search Controls - allows user to make selections to search for a user.
 * The onConfirm callback is invoked when a selection is confirmed.
 */
class SearchControls extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchText: null,
            location: null,
            field: null,
            fieldsOptions: [{id: null, name: "All Creative Fields"}]
        };
        this.setSearchText = this.setSearchText.bind(this);  // stores user's search input
        this.setLocation = this.setLocation.bind(this);
        this.setField = this.setField.bind(this);
        this.countryOptions = [{id: null, name: "All locations"}, ...COUNTRY_LIST];
    }

    componentDidMount() {
        const {fields} = this.props;
        if (!fields.length) {
            this.props.fetchFields().then(() => {
                this.setState({fieldsOptions: [...this.state.fieldsOptions, ...this.props.fields]});
            });
        } else {
            this.setState({fieldsOptions: [...this.state.fieldsOptions, ...this.props.fields]});
        }
    }

    setSearchText(e) {
        this.setState({searchText: e.target.value});
    }

    setLocation(location) {
        this.setState({location: location});
    }

    setField(field) {
        this.setState({field: field});
    }

    render() {
        const {onConfirm, error} = this.props;
        const {searchText, location, field, fieldsOptions} = this.state;
        return (
            <div className={"search-controls"}>
                <div className={'instructions'}>
                    To search for a user, please enter First Name or Last Name.
                    {error &&
                    <div className={"error-message"}>
                        A name is required for the search. Please enter a name.
                    </div>}
                </div>
                <div className={'controls'}>
                    <input type="text" onChange={this.setSearchText} placeholder={"e.g. Joe Smith"}/>
                    <button onClick={onConfirm.bind(this, {
                        searchText: searchText,
                        location: location,
                        field: field
                    })}> Search
                    </button>
                </div>
                <div className={'advanced-search'}>
                    <SimpleDropDown className={"location-drop-down"}
                                    selected={this.countryOptions[0]}
                                    options={this.countryOptions}
                                    onSelect={this.setLocation}/>
                    <SimpleDropDown className={"fields-drop-down"}
                                    selected={fieldsOptions[0]}
                                    options={fieldsOptions}
                                    onSelect={this.setField}/>
                </div>
            </div>);
    }
}

SearchControls.propTypes = {
    onConfirm: PropTypes.func
};

const mapStateToProps = state => {
    return {
        fields: state.fields
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchFields: () => dispatch(fetchFields())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchControls);
