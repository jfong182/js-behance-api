import React, {Component} from 'react';
import '../stylesheets/SearchView.scss';
import {fetchUserList} from "../../actions/UserListActionsWrapper";
import {resetUserList} from "../../actions/UserListActions";
import connect from "react-redux/es/connect/connect";
import {PulseLoader} from 'react-spinners';
import SearchControls from "./SearchControls";
import SearchResults from "./SearchResults";

// Enums for indicating the search status.
const SEARCH_STATUS = {"INITIAL": 0, "IN_PROGRESS": 1, "COMPLETED": 2};

/**
 * Search View - contains user search functionality and displays matching results.
 */
class SearchView extends Component {
    constructor(props) {
        super(props);
        this.validate = this.validate.bind(this);
        this.state = {
            searchStatus: this.props.userList.length ? SEARCH_STATUS.COMPLETED : SEARCH_STATUS.INITIAL, // stores the status for a search,
            error: false // a flag for search input validation error
        };
    }

    validate(params) {
        if (!params.searchText) {
            this.props.resetUsers();
            this.setState({searchStatus: SEARCH_STATUS.INITIAL, error: true});
        } else {
            this.fetchUsers(params);
        }
    }

    fetchUsers(searchText, location, field) {
        this.setState({searchStatus: SEARCH_STATUS.IN_PROGRESS, error: false});

        this.props.fetchUsers(searchText, location, field)
            .then(() => this.setState({searchStatus: SEARCH_STATUS.COMPLETED}));
    }

    render() {
        const {searchStatus, error} = this.state;

        return (
            <div className={"search-view"}>
                {searchStatus === SEARCH_STATUS.IN_PROGRESS &&
                <div className={"loader-overlay"}>
                    <PulseLoader className={"loader"} color={"#16a6d7"} loading={true} size={20}/>
                </div>}
                <div className={"search-view__top-section"}>
                    <h1>Behance User Finder&#8194;&#128269;</h1>
                    <div className={"search_view__intro"}>
                        This page allows searching for users and their profiles on
                        <a className={"company-name"} href={"https://www.behance.net/"} target={"_blank"}>Behance</a>,
                        one of the most popular websites for front-end and art portfolios.
                    </div>
                </div>
                <SearchControls onConfirm={this.validate} error={error}/>
                {searchStatus === SEARCH_STATUS.COMPLETED &&
                <SearchResults {...this.props}/>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        userList: state.userList,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchUsers: (params) => dispatch(fetchUserList(params)),
        resetUsers: () => dispatch(resetUserList())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchView);
