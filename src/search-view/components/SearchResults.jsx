import React, {Component} from "react";
import '../stylesheets/SearchResults.scss';
import connect from "react-redux/es/connect/connect";
import PropTypes from "prop-types";

/**
 * SearchResults - displays a list of users based on the search criteria.
 * It redirects to the user's profile page upon clicking on a user row.
 */
class SearchResults extends Component {

    render() {
        const {userList} = this.props;
        return (
            <div className={"search-results"}>
                <div className={"no-of-users"}>
                    We have found {userList.length || "0"} matching users. <b>Click on the user to go to
                    user's profile
                    page.</b>
                </div>
                {userList.length > 0 &&
                <table>
                    <thead>
                    <tr>
                        <th className={"first-name-col"}>First Name</th>
                        <th className={"last-name-col"}>Last Name</th>
                        <th className={"location-col"}>Location</th>
                        <th className={"no-of-followers-col"}>Number of followers</th>
                    </tr>
                    </thead>
                    <tbody>
                    {userList.map((user, i) =>
                        (<tr key={i} onClick={() => this.props.history.push(`/profile/${user.username}`)}>
                            <td>{user.firstName}</td>
                            <td>{user.lastName}</td>
                            <td>{user.location}</td>
                            <td>{user.followers}</td>
                        </tr>))
                    }
                    </tbody>
                </table>}
            </div>
        );
    }
}

SearchResults.propTypes = {
    history: PropTypes.object // the "history" object from React Router which contains navigating functions
};

const mapStateToProps = state => {
    return {
        userList: state.userList,
    };
};

export default connect(mapStateToProps, null)(SearchResults);
