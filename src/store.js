import {applyMiddleware, createStore} from "redux";
import rootReducer from "./reducers";
import thunk from 'redux-thunk';

/**
 * Create redux store for the application
 */
export default createStore(
    rootReducer,
    applyMiddleware(thunk)
);
