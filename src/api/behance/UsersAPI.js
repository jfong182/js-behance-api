import axios from 'axios';
import {API_KEY} from "config";
import {sanitize} from "../../utils/URLUtils";

/**
 * Gets a list of users from Behance based on search criteria and parses relevant properties.
 * @param params
 * @returns {Promise<*>}
 */
export const getUserList = async (params) => {
    const {searchText, location, field} = params;

    // build query params
    let queryList = [`client_id=${API_KEY}`];
    if (searchText) {
        queryList.push(`q=${sanitize(searchText)}`);
    }
    if (location) {
        queryList.push(`country=${sanitize(location)}`);
    }
    if (field) {
        queryList.push(`field=${sanitize(field)}`);
    }

    let queryStr = queryList.join('&');

    const res = await axios.get(`/api/v2/users?${queryStr}`);
    const users = res.data['users'];

    if (!users) {
        return null;
    } else {
        return users.map(user => {
            return {
                firstName: user['first_name'],
                lastName: user['last_name'],
                username: user['username'],
                location: user['location'],
                followers: user['stats'] && user['stats']['followers']
            }
        });
    }
};

/**
 * Gets user's basic information from Behance and parses relevant properties.
 * @param username
 * @returns {Promise<*>}
 */
export const getBasicInfo = async (username) => {
    const res = await axios.get(`/api/v2/users/${username}?client_id=${API_KEY}`);
    const user = res.data['user'];

    if (!user) {
        return null;
    } else {
        return {
            firstName: user['first_name'],
            lastName: user['last_name'],
            location: user['location'],
            company: user['company'],
            occupation: user['occupation'],
            imageUrl: user['images'] && user['images']['230'],
            fields: user['fields'],
            socialLinks: user['social_links']
        };
    }
};

/**
 * Gets user's stats from Behance and parses relevant properties.
 * @param username
 * @returns {Promise<*>}
 */
export const getStats = async (username) => {
    const res = await axios.get(`/api/v2/users/${username}/stats?client_id=${API_KEY}`);
    const stats = res.data['stats'];

    if (!stats) {
        return null;
    } else {
        const allTime = stats['all_time'];
        const today = stats['today'];

        return {
            allTimeProjectViews: allTime && allTime['project_views'],
            allTimeProjectAppreciations: allTime && allTime['project_appreciations'],
            allTimeProjectComments: allTime && allTime['project_comments'],
            allTimeProfileViews: allTime && allTime['profile_views'],
            todayProjectViews: today && today['project_views'],
            todayProjectAppreciations: today && today['project_appreciations'],
            todayProjectComments: today && today['project_comments'],
            todayProfileViews: today && today['profile_views']
        };
    }
};

/**
 * Gets user's followers from Behance and parses relevant properties.
 * @param username
 * @returns {Promise<*>}
 */
export const getFollowers = async (username) => {
    const res = await axios.get(`/api/v2/users/${username}/followers?client_id=${API_KEY}`);
    const followers = res.data['followers'];

    if (!followers) {
        return null;
    } else {
        return followers.map((f) => {
            return {
                displayName: f['display_name'],
                location: f['location'],
                company: f['company'],
                occupation: f['occupation'],
                url: f['url'],
                noOfFollowers: f['stats'] && f['stats']['followers']
            }
        });
    }
};

/**
 * Gets user's followees from Behance and parses relevant properties.
 * @param username
 * @returns {Promise<null>}
 */
export const getFollowing = async (username) => {
    const res = await axios.get(`/api/v2/users/${username}/following?client_id=${API_KEY}`);
    const following = res.data['following'];

    if (!following) {
        return null;
    } else {
        return following.map((f) => {
            return {
                displayName: f['display_name'],
                location: f['location'],
                company: f['company'],
                occupation: f['occupation'],
                url: f['url'],
                noOfFollowers: f['stats'] && f['stats']['followers']
            }
        });
    }
};

/**
 * Gets user's projects from Behance and parses relevant properties.
 * @param username
 * @returns {Promise<*>}
 */
export const getProjects = async (username) => {
    const res = await axios.get(`/api/v2/users/${username}/projects?client_id=${API_KEY}`);
    const projects = res.data['projects'];

    if (!projects) {
        return null;
    } else {
        return projects.map((p) => {
            return {
                name: p['name'],
                publishedOn: p['published_on'],
                url: p['url'],
                imageUrl: p['covers'] && p['covers']['115'],
                noOfViews: p['stats'] && p['stats']['views'],
                noOfAppreciations: p['stats'] && p['stats']['appreciations']
            }
        });
    }
};

/**
 * Gets user's work experience from Behance and parses relevant properties.
 * @param username
 * @returns {Promise<*>}
 */
export const getWorkExperience = async (username) => {
    const res = await axios.get(`/api/v2/users/${username}/work_experience?client_id=${API_KEY}`);
    const workExperience = res.data['work_experience'];

    if (!workExperience) {
        return null;
    } else {
        return workExperience.map((p) => {
            return {
                position: p['position'],
                organization: p['organization'],
                location: p['location'],
                startDate: p['start_date'],
                endDate: p['end_date']
            }
        });
    }
};


