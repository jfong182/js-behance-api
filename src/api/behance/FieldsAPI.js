import axios from 'axios';
import {API_KEY} from "config";

/**
 * Gets all Creative Fields from Behance
 * @returns {Promise<*>}
 */
export const getFields = async () => {
    const res = await axios.get(`/api/v2/fields?client_id=${API_KEY}`);
    return res.data['fields'];
};
