import React from 'react'
import ReactDOM from 'react-dom'
import {AppContainer} from 'react-hot-loader'
import './main.scss';
import Routes from './routes'
import {Provider} from 'react-redux';
import store from 'store';

/**
 * Entry point for application
 */
const renderApp = (Component) => {
    ReactDOM.render(
        <Provider store={store}>
            <AppContainer>
                <Component/>
            </AppContainer>
        </Provider>,
        document.getElementById('app')
    );
};

renderApp(Routes);

// Hot Module Replacement
if (module.hot) {
    module.hot.accept('./routes', () => {
        renderApp(require('./routes').default);
    })
}
