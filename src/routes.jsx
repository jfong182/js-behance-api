import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import SearchView from './search-view/components/SearchView';
import ProfileView from "./profile-view/components/ProfileView";

/**
 * Routes between different views.
 */
const Routes = () => (
    <Router>
        <div className={"container"}>
            <Route exact path="/" component={SearchView}/>
            <Route exact path="/profile/:username" render={(props) => <ProfileView {...props} />}/>
        </div>
    </Router>
);

export default Routes;
