/**
 * Mock data for a user's projects
 */
export const projectList = [{
    "id": 1,
    "name": "project1",
    "published_on": 1536784113,
    "created_on": 1536728620,
    "modified_on": 1539288235,
    "url": "https:\/\/project1.com\/link",
    "slug": "PROJECT1",
    "privacy": "public",
    "fields": ["Branding", "Graphic Design"],
    "covers": {
        "230": "https:\/\/project1.com\/230\/1.jpg",
    },
    "mature_content": 0,
    "mature_access": "allowed",
    "owners": [{
        "id": 5,
        "first_name": "user5 First Name",
        "last_name": "user5 Last Name",
        "username": "user5",
        "city": "",
        "state": "",
        "country": "Germany",
        "location": "Germany",
        "company": "user5 Company",
        "occupation": "",
        "created_on": 1539800311,
        "url": "",
        "images": {},
        "display_name": "user1",
        "fields": [],
        "has_default_image": 1,
        "website": "",
        "stats": {"followers": 0, "following": 15, "appreciations": 0, "views": 0, "comments": 0}
    }],
    "stats": {"views": 6268, "appreciations": 1362, "comments": 48},
    "conceived_on": 1536710400,
    "colors": [{"r": 142, "g": 1, "b": 180}]
},
    {
        "id": 2,
        "name": "project2",
        "published_on": 1536784113,
        "created_on": 1536728620,
        "modified_on": 1539288235,
        "url": "https:\/\/project2.com\/link",
        "slug": "PROJECT1",
        "privacy": "public",
        "fields": ["Branding", "Graphic Design"],
        "covers": {
            "230": "https:\/\/project2.com\/230\/1.jpg",
        },
        "mature_content": 0,
        "mature_access": "allowed",
        "owners": [{
            "id": 5,
            "first_name": "user5 First Name",
            "last_name": "user5 Last Name",
            "username": "user5",
            "city": "",
            "state": "",
            "country": "Germany",
            "location": "Germany",
            "company": "user5 Company",
            "occupation": "",
            "created_on": 1539800311,
            "url": "",
            "images": {},
            "display_name": "user5",
            "fields": [],
            "has_default_image": 1,
            "website": "",
            "stats": {"followers": 0, "following": 15, "appreciations": 0, "views": 0, "comments": 0}
        }],
        "stats": {"views": 100, "appreciations": 200, "comments": 48},
        "conceived_on": 1536710400,
        "colors": [{"r": 142, "g": 1, "b": 180}]
    }
];

