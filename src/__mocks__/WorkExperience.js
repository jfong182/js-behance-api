/**
 * Mock data for a user's work experience
 */
export const workExperience = [
    {
        "position": "Art Director",
        "organization": "CompanyA",
        "location": "Milan, Italy"
    }, {
        "position": "Waiter",
        "organization": "CompanyB",
        "location": "Milan, Italy"
    }
];
