/**
 * Mock data for a user's followees
 */
export const following = [{
    "id": 3,
    "first_name": "user3 First Name",
    "last_name": "user3 Last Name",
    "username": "user3",
    "city": "Chicago",
    "state": "IL",
    "country": "United States",
    "location": "Chicago, United States",
    "company": "",
    "occupation": "Senior Graphic Designer",
    "created_on": 3201834538,
    "url": "http:\/\/user3.com",
    "images": {
        "230": "https:\/\/user1/profile.png",
    },
    "display_name": "user3",
    "fields": ["Art Direction"],
    "has_default_image": 0,
    "website": "",
    "stats": {"followers": 82011, "following": 42, "appreciations": 99050, "views": 1744189, "comments": 2160}
},
    {
        "id": 4,
        "first_name": "user4 First Name",
        "last_name": "user4 Last Name",
        "username": "user4",
        "city": "",
        "state": "",
        "country": "China",
        "location": "China",
        "company": "",
        "occupation": "Student",
        "created_on": 1201834538,
        "url": "",
        "images": {
            "230": "https:\/\/user4/profile.png",
        },
        "display_name": "user4",
        "fields": ["Copywriting"],
        "has_default_image": 0,
        "website": "",
        "stats": {"followers": 3, "following": 4, "appreciations": 1, "views": 10, "comments": 10}
    }
];
