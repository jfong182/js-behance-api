/**
 * Mock data for a user's stats
 */
export const stats = {
    "today":
        {
            "project_views":
                131, "project_appreciations":
                7, "project_comments":
                0, "profile_views":
                16
        }
    ,
    "all_time":
        {
            "project_views":
                147111, "project_appreciations":
                14459, "project_comments":
                645, "profile_views":
                6943
        }
};
