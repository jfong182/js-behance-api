/**
 * Mock data for a user's followers
 */
export const followers = [{
    "id": 1,
    "first_name": "user1 First Name",
    "last_name": "user1 Last Name",
    "username": "user1",
    "city": "",
    "state": "",
    "country": "Germany",
    "location": "Germany",
    "company": "user1 Company",
    "occupation": "",
    "created_on": 1539800311,
    "url": "",
    "images": {},
    "display_name": "user1",
    "fields": [],
    "has_default_image": 1,
    "website": "",
    "stats": {"followers": 0, "following": 15, "appreciations": 0, "views": 0, "comments": 0}
},
    {
        "id": 2,
        "first_name": "user2 First Name",
        "last_name": "user2 Last Name",
        "username": "user2",
        "city": "",
        "state": "",
        "country": "Taiwan",
        "location": "Taiwan",
        "company": "user2 Company",
        "occupation": "",
        "created_on": 1539800311,
        "url": "",
        "images": {
            "230": "",
        },
        "display_name": "user2",
        "fields": [],
        "has_default_image": 0,
        "website": "",
        "stats": {"followers": 1000, "following": 15, "appreciations": 1, "views": 2, "comments": 0}
    }
];
