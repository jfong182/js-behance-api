/**
 * Mock data for a user's basic info
 */
export const basicInfo = {
    "id": 5,
    "first_name": "user5 First Name",
    "last_name": "user5 Last Name",
    "username": "user5",
    "city": "",
    "state": "",
    "country": "Germany",
    "location": "Germany",
    "company": "user5 Company",
    "occupation": "",
    "created_on": 1539800311,
    "url": "",
    "images": {},
    "display_name": "user5",
    "fields": [],
    "has_default_image": 1,
    "website": "",
    "stats": {"followers": 0, "following": 15, "appreciations": 0, "views": 0, "comments": 0},
    "twitter": "",
    "links": [],
    "sections": {"\u041e\u0431\u043e \u043c\u043d\u0435": "Designer?"},
    "social_links": [{
        "social_id": 12,
        "url": "http:\/\/instagram.com\/user5",
        "service_name": "Instagram",
        "value": "user5",
        "isInstagram": true,
        "social_network_type": "isInstagram"
    }, {
        "social_id": 20,
        "url": "http:\/\/soundcloud.com\/user5",
        "service_name": "SoundCloud",
        "value": "user5",
        "isSoundCloud": true,
        "social_network_type": "isSoundCloud"
    }],
    "has_social_links": true
};
