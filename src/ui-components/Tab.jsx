import React, {Component} from 'react';
import PropTypes from 'prop-types';

/**
 * UI-Component: Tab - returns a Tab element used in the Tabs UI component.
 */
export default class Tab extends Component {

    handleClick() {
        this.props.onClick(this.props.label);
    }

    getClassNames() {
        const {label, activeTab} = this.props;
        return `tab-list-item${activeTab === label ? ' tab-list-active' : ''}`;
    }

    render() {
        return (
            <li className={this.getClassNames()} onClick={() => this.handleClick()}>
                {this.props.label}
            </li>
        );
    }
}

Tab.propTypes = {
    label: PropTypes.string, // the displayed text in the Tab
    activeTab: PropTypes.string, // the displayed text for the active Tab
    onClick: PropTypes.func // the onClick callback when the Tab receives a click
};
