import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './stylesheets/Tabs.scss';
import Tab from './Tab';

/**
 * UI-Component: Tabs. It controls a list of tabs and the display of different content for the corresponding tab.
 */
export default class Tabs extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeTab: this.props.children[0].props.label,
        };
    }

    onClickTabItem(tab) {
        this.setState({activeTab: tab});
    }

    render() {
        const {children} = this.props;
        const {activeTab} = this.state;

        return (
            <div className="tabs">
                <ol className="tab-list">
                    {children.map((child) => {
                        const {label} = child.props;
                        return (
                            <Tab key={label}
                                 label={label}
                                 activeTab={activeTab}
                                 onClick={(tab) => this.onClickTabItem(tab)}/>
                        );
                    })}
                </ol>
                <div className="tab-content">
                    {children.map((child, i) => {
                        return (child.props.label === activeTab) ?
                            (<div key={i}>{child}</div>) :
                            (<div key={i} className={"hidden"}>{child}</div>)  // hide instead of unmounting component
                    })}
                </div>
            </div>
        );
    }
}

Tabs.propTypes = {
    children: PropTypes.array // a list of React children within the Tabs component
};
