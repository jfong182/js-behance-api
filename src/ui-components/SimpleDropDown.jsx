import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './stylesheets/SimpleDropDown.scss';

/**
 * UI-Component: Simple Drop Down
 */
export default class SimpleDropDown extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            selected: props.selected
        };

        this.containerRef = null;
        this.setRef = this.setRef.bind(this);
        this.onTrigger = this.onTrigger.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);

    }

    componentDidMount() {
        document.addEventListener('click', this.handleOutsideClick);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleOutsideClick);
    }

    setRef(node) {
        this.containerRef = node;
    }

    // if user clicks outside the dropdown, the dropdown menu should be closed.
    handleOutsideClick(event) {
        if (this.containerRef && !this.containerRef.contains(event.target)) {
            this.setState({isOpen: false});
        }
    }

    onTrigger() {
        this.setState({isOpen: !this.state.isOpen});
    }

    onSelect(option) {
        this.props.onSelect(option.id);
        this.setState({selected: option, isOpen: false});
    }

    render() {
        const {selected, isOpen} = this.state;
        const {options} = this.props;

        return (
            <div ref={this.setRef} className={"simple-drop-down"}>
                <div className={"simple-drop-down__trigger"} onClick={this.onTrigger}>
                    <span className={"selected"}>{selected.name}</span>
                    {isOpen ? (<span className={"caret"}>&#x25B2;</span>) : (<span className={"caret"}>&#x25BC;</span>)}
                </div>
                {isOpen && (
                    <div className={"menu"}>
                        {options && options.map((op, i) => {
                            return (
                                <li key={i} onClick={this.onSelect.bind(this, op)}>{op.name}</li>
                            )
                        })}
                    </div>)
                }
            </div>
        );
    }
}

const Item = PropTypes.shape({id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]), name: PropTypes.string});

SimpleDropDown.propTypes = {
    selected: Item,
    options: PropTypes.arrayOf(Item),
    onSelect: PropTypes.func
};
