import {ActionTypes} from 'actions/ActionTypes';

/**
 * Fields reducer
 */
export const updateFields = (state = [], action) => {
    switch (action.type) {
        case ActionTypes.FETCH_FIELDS_SUCCESS:
            return action.payload;
        case ActionTypes.FETCH_FIELDS_ERROR:
            return [];
        default:
            return state;
    }
};
