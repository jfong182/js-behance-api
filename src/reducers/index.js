import {combineReducers} from "redux";
import {updateUserList} from './UserListReducer';
import {updateUser} from "./UserReducer";
import {updateFields} from "./FieldsReducer";

/**
 * Root reducer which combines all the reducers.
 */
export default combineReducers({
    userList: updateUserList,
    user: updateUser,
    fields: updateFields
});
