import {ActionTypes} from '../actions/ActionTypes';

export const initialState = {
    basicInfo: null,
    stats: null,
    projects: [],
    workExperience: [],
    followers: [],
    following: []
};

/**
 * User Reducer
 */
export const updateUser = (state = {...initialState}, action) => {
    switch (action.type) {
        case ActionTypes.RESET_USER:
            return {...initialState};
        case ActionTypes.FETCH_BASIC_INFO_SUCCESS:
            return {...state, basicInfo: action.payload};
        case ActionTypes.FETCH_BASIC_INFO_ERROR:
            return {...state, basicInfo: null};
        case ActionTypes.FETCH_STATS_SUCCESS:
            return {...state, stats: action.payload};
        case ActionTypes.FETCH_STATS_ERROR:
            return {...state, stats: null};
        case ActionTypes.FETCH_PROJECTS_SUCCESS:
            return {...state, projects: action.payload};
        case ActionTypes.FETCH_PROJECTS_ERROR:
            return {...state, projects: []};
        case ActionTypes.FETCH_WORK_EXPERIENCE_SUCCESS:
            return {...state, workExperience: action.payload};
        case ActionTypes.FETCH_WORK_EXPERIENCE_ERROR:
            return {...state, workExperience: []};
        case ActionTypes.FETCH_FOLLOWERS_SUCCESS:
            return {...state, followers: action.payload};
        case ActionTypes.FETCH_FOLLOWERS_ERROR:
            return {...state, followers: []};
        case ActionTypes.FETCH_FOLLOWING_SUCCESS:
            return {...state, following: action.payload}
        case ActionTypes.FETCH_FOLLOWING_ERROR:
            return {...state, following: []};
        default:
            return state;
    }
};
