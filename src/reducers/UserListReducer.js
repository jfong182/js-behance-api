import {ActionTypes} from '../actions/ActionTypes';

/**
 * UserList reducer
 */
export const updateUserList = (state = [], action) => {
    switch (action.type) {
        case ActionTypes.FETCH_USER_LIST_SUCCESS:
            return action.payload;
        case ActionTypes.FETCH_USER_LIST_ERROR:
        case ActionTypes.RESET_USER_LIST:
            return [];
        default:
            return state;
    }
};
