"use strict";
const webpack = require('webpack');
const path = require('path');
const loadersConf = require('./webpack.loaders');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

// host and port for local development
const HOST = process.env.HOST || "localhost";
const PORT = process.env.PORT || "3000";

/**
 * Webpack configurations for local development.
 */
module.exports = {
    entry: [
        './src/index.jsx',
    ],
    output: {
        publicPath: '/',
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js'
    },
    module: {
        rules: loadersConf
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: [
            path.join(__dirname, "src"),
            path.join(__dirname, "node_modules"),
        ]
    },
    devServer: {
        contentBase: "./public",

        // bypass CORS for local development
        proxy: {
            '/api/**': {
                target: 'https://api.behance.net/',
                pathRewrite: {'^/api': ''},
                secure: false,
                logLevel: 'debug',
                changeOrigin: true
            }
        },
        noInfo: true,
        hot: true,
        inline: true,
        historyApiFallback: true,
        port: PORT,
        host: HOST
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new ExtractTextPlugin({
            filename: 'style.css',
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            files: {
                css: ['style.css'],
                js: ["bundle.js"],
            }
        }),
    ]
};
